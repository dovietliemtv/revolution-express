import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import del from 'del';
import imagemin from 'gulp-imagemin';

const paths = {
    styles: {
        src: 'themes/custom/assets/sass/*.scss',
        dest: 'themes/custom/assets/css/'
    },
    scripts: {
        src: 'themes/custom/assets/js/*.js',
        dest: 'themes/custom/assets/script/'
    },
    image: {
        src: 'themes/custom/assets/imagesOrigin/**',
        dest: 'themes/custom/assets/images/'
    }
};

/*
 * For small tasks you can export arrow functions
 */
export const clean = () => del(['css']);

/*
 * You can also declare named functions and export them as tasks
 */
export function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        // pass in options to the stream
        .pipe(rename({
            basename: 'main',
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.styles.dest));
}

export function scripts() {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(babel())
        .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
}

export function image() {
    gulp.src(paths.image.src)
    .pipe(imagemin())
    .pipe(gulp.dest(paths.image.dest))
}

/*
 * You could even use `export as` to rename exported tasks
 */
function watchFiles() {
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch(paths.styles.src, styles);
}
export { watchFiles as watch };

const build = gulp.series(clean, gulp.series(styles, scripts));
/*
 * Export a default task
 */
export default build;